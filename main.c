#include <avr/io.h> //standard include for ATMega16
#include <string.h>
#include <util/delay.h> //standard include for ATMega16
#include <avr/interrupt.h>
#include <stdlib.h>

/* Sets the baud rate */
#define UART_BAUD_RATE      9600
#define UART_BAUD_SELECT (F_CPU/16/UART_BAUD_RATE-1)

#include <stdio.h>
#include <avr/io.h>
#include <inttypes.h>

#define UBRR0L  UBRRL
#define UBRR0H  UBRRH
#define UCSR0B  UCSRB
#define UCSR0A  UCSRA
#define UDR0  UDR
#define RXEN0	RXEN
#define TXEN0	TXEN
#define RXC0	RXC
#define UDRE0	UDRE

#define BTKEY_PORT	PORTC
#define BTKEY_DDR	DDRC
#define BTKEY_PIN	PINC
#define BTKEY_BIT	_BV(5)

#define BTLED_PORT	PORTB
#define BTLED_DDR	DDRB
#define BTLED_PIN	PINB
#define BTLED_BIT	_BV(1)

#define SIGNAL_PORT	PORTD
#define SIGNAL_DDR	DDRD
#define SIGNAL_PIN	PIND
#define SIGNAL_BIT	_BV(2)

#define TELE_PORT	PORTD
#define TELE_DDR	DDRD
#define TELE_PIN	PIND
#define TELE_BIT	_BV(3)

#define MAX_CHANNELS 16
#define MAX_BYTES	20

#define TIMER_ENABLE() do{TCCR2 =  _BV(WGM21) | _BV(CS20);}while(0); // 0 prescaler and CTC mode
#define TIMER_DISABLE() do{TCCR2 = 0x00;}while(0); 

//This function is used to initialize the USART
//at a given UBRR value
void USARTInit(uint16_t ubrr_value)
{
	UBRR0L = ubrr_value;
	UCSR0B=(1<<RXEN0)|(1<<TXEN0)|(1<<TXCIE);
}

//This function is used to read the available data
//from USART. This function will wait untill data is
//available.
char USARTReadChar()
{
	//Wait untill a data is available

	while(!(UCSR0A & (1<<RXC0)))
	{
		//Do nothing
	}

	//Now USART has got data from host
	//and is available is buffer

	return UDR0;
}

//This fuction writes the given "data" to
//the USART which then transmit it via TX line
void USARTWriteChar(char data)
{
	//Wait untill the transmitter is ready

	while(!(UCSR0A & (1<<UDRE0)))
	{
		//Do nothing
	}

	//Now write the data to USART buffer

	UDR0=data;
}


void USARTWriteString(char *data)
{
	while(*data)
		USARTWriteChar(*data++);
}

volatile unsigned char ind=0; //number of channel(0-6) 
unsigned int puls_array[7];

volatile unsigned char ppm = 0;
enum {STATE_INIT, STATE_WAIT, STATE_MEASURE};
uint8_t intstate = STATE_INIT;
uint8_t channel = 0;

volatile uint8_t bitvals[2];
volatile uint8_t bytes[MAX_BYTES];
volatile uint8_t *uart_data_ptr;
volatile uint8_t current = 2;
volatile uint8_t bitcount = 0;

ISR(INT0_vect)
{
	TCNT2 = 100;
	TIMER_ENABLE();
	GICR &= ~_BV(INT0);
	current = 2;
	bitcount = 0;
}

ISR(USART_TXC_vect)
{
	if ( uart_data_ptr == NULL || uart_data_ptr == bytes + 16)
	{
		TELE_PORT ^= TELE_BIT;
		TELE_PORT ^= TELE_BIT;
		return;
	}
	UDR0 = *uart_data_ptr++;
}


ISR(TIMER2_COMP_vect)
{
	TELE_PORT ^= TELE_BIT;
	bitvals[1] <<= 1;
	bitvals[1] |= !!(bitvals[0] & _BV(7));
	bitvals[0] <<= 1;
	bitvals[0] |= !!(SIGNAL_PIN & SIGNAL_BIT);
	bitcount++;


	if ( (bitvals[0] == 0xff && bitvals[1] == 0xff ) || current > MAX_BYTES ) {
		GICR |= _BV(INT0); // Enable interrupt
		TIMER_DISABLE();
	} else {
		if (bitcount == 9) {
			bytes[current] = bitvals[0];
			if(current == 2)
			{
				uart_data_ptr = bytes + 1;
				UDR0 = bytes[0];
			}

			current++;
		} else if ( bitcount == 11 ) {
			bitcount = 0;
		}
	}
	TELE_PORT ^= TELE_BIT;
}



void TimerInit(void)
{
	OCR2 = 127;
	TCCR2 =  _BV(WGM21) | _BV(CS20); // 0 prescaler and CTC mode
	TIMSK = _BV(OCIE2);
}


void IntInit()
{
	//GICR = _BV(INT0);
	MCUCR = _BV(ISC01); // Falling edge
}

int main()
{
	bytes[0] = 'B';
	bytes[1] = 'T';
	BTKEY_DDR |= BTKEY_BIT;
	BTLED_DDR |= BTLED_BIT;
	BTLED_PORT &= ~BTLED_BIT;

	TELE_DDR |= TELE_BIT;


	TimerInit();
	IntInit();
	USARTInit(UART_BAUD_SELECT);
	sei();

	while(1);

	return 0;
}


